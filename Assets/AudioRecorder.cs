using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AudioRecorder : MonoBehaviour
{
    AudioClip reco;
    AudioClip recording;
    //Keep this one as a global variable (outside the functions) too and use GetComponent during start to save resources
    AudioSource audioSource;
    private float startRecordingTime;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void onClick()
    {
        SavWav ss_o = new SavWav();
        int frequency = 44100; //Wav format frequency
        int numberOfSecondsToRecord = 10;
        var rec = Microphone.Start("Microphone (USB Audio)", true, numberOfSecondsToRecord, frequency);
        Thread.Sleep(numberOfSecondsToRecord * 1000); //To miliseconds
        if (rec != null)
        {
            ss_o.Save(Application.persistentDataPath + "/test.wav", rec);
        }
    }
    public void OnPointerUp()
    {
        //End the recording when the mouse comes back up, then play it
        Microphone.End("");

        //Trim the audioclip by the length of the recording
        AudioClip recordingNew = AudioClip.Create(recording.name, (int)((Time.time - startRecordingTime) * recording.frequency), recording.channels, recording.frequency, false);
        float[] data = new float[(int)((Time.time - startRecordingTime) * recording.frequency)];
        recording.GetData(data, 0);
        recordingNew.SetData(data, 0);
        this.recording = recordingNew;

        //Play recording
        audioSource.clip = recording;
       //audioSource.Play();

    }

    public AudioClip endRecording()
    {
        Microphone.End("");

        //Trim the audioclip by the length of the recording
        AudioClip recordingNew = AudioClip.Create(recording.name, (int)((Time.time - startRecordingTime) * recording.frequency), recording.channels, recording.frequency, false);
        float[] data = new float[(int)((Time.time - startRecordingTime) * recording.frequency)];
        recording.GetData(data, 0);
        recordingNew.SetData(data, 0);
        this.recording = recordingNew;

        //Play recording
        //audioSource.clip = recording;
        return recording;
    }

    public void OnPointerDown()
    {
        //Get the max frequency of a microphone, if it's less than 44100 record at the max frequency, else record at 44100
        int minFreq;
        int maxFreq;
        int freq = 44100;
        Microphone.GetDeviceCaps("", out minFreq, out maxFreq);
        if (maxFreq < 44100)
            freq = maxFreq;

        //Start the recording, the length of 300 gives it a cap of 5 minutes
        recording = Microphone.Start("", false, 120, 44100);
        startRecordingTime = Time.time;
    }
}
