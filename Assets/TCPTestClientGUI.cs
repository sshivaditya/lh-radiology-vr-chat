﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System.Collections;
using System.Linq;

public class TCPTestClientGUI : MonoBehaviour
{
    private List<TCPTestClient> clients = new List<TCPTestClient>();
    private TCPTestServer _server;
    private TCPTestClient _client;
    public TMP_InputField tmp_input;
    public InputField IPInputField;
    public InputField PortInputField;
    public InputField MessageInputField;
    public AudioRecorder sound;
    SavWav ss_o = new SavWav();
    public Text TextWindow;
    private string text;

    [SerializeField] private Transform m_ContentContainer;
    [SerializeField] private GameObject m_ItemPrefab;
    [SerializeField] private int m_ItemsToGenerate;

    public AudioClip sss;
    public AudioSource audioSource;

    private object cacheLock = new object();
    private string cache;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        sound = GetComponent<AudioRecorder>();
        _server = GetComponent<TCPTestServer>();
        _server.OnLog += OnServerReceivedMessage;
        _client = GetComponent<TCPTestClient>();
        _client.OnConnected += OnClientConnected;
        _client.OnDisconnected += OnClientDisconnected;
        _client.OnMessageReceived += OnClientReceivedMessage;
        _client.OnLog += OnClientLog;
    }

    

    public void Start()
    {
        
    }

    private void Update()
    {
        lock (cacheLock)
        {
            if (!string.IsNullOrEmpty(cache))
            {
                TextWindow.text += string.Format("{0}", cache);
                var item_go = Instantiate(m_ItemPrefab);
                // do something with the instantiated item -- for instance
                item_go.GetComponentInChildren<Text>().text = string.Format("{0}", cache);
                //parent the item to the content container
                item_go.transform.SetParent(m_ContentContainer);
                //reset the item's scale -- this can get munged with UI prefabs
                item_go.transform.localScale = Vector2.one;
                item_go.transform.localPosition += new Vector3(0, 0, 1325);
                cache = null;
            }
        }
    }

    public void StartServer()
    {
        if (!_server.IsConnected)
        {
            _server.IPAddress = IPInputField.text;
            int.TryParse(PortInputField.text, out _server.Port);
            _server.StartServer();
        }
    }

    public void ConnectClient()
    {
        if (!_client.IsConnected)
        {
            _client.IPAddress = IPInputField.text;
            int.TryParse(PortInputField.text, out _client.Port);
            _client.ConnectToTcpServer();
        }
    }

    public void DisconnectClient()
    {
        Debug.Log(_client.IsConnected);
        if (_client.IsConnected)
        {
            _client.CloseConnection();
        }
    }

    public void onPointerDown()
    {
        sound.OnPointerDown();
    }

    public void OnPointerUp()
    {
        //Send Audio to the server
        if (_client.IsConnected)
        {
            sss = sound.endRecording();
        }
        audioSource.clip = sss;
        audioSource.Play();
        //float[] dataAudio = new float[10000];
        //_ = sss.GetData(dataAudio, 10000);
        ss_o.Save(Application.persistentDataPath + "/test.wav", sss);
        //Debug.Log(dataAudio);
        //string result = $"[{string.Join(",",dataAudio)}]";
        StartCoroutine(StartUploadCoroutine(Application.persistentDataPath + "/test.wav"));
        _client.SendMessage(string.Format("{0}:", "Audio Clip Sent"));
    }
    
    IEnumerator StartUploadCoroutine(string filePath)
    {
        StartCoroutine(UploadCoroutine(filePath));
        Debug.Log(filePath);
        yield return Enumerable.Empty<string>();
    }

    IEnumerator UploadCoroutine(string filePath)
    {
        WWWForm form = new WWWForm();
        Debug.Log(filePath);
        form.AddBinaryData("audFile", File.ReadAllBytes(filePath));
        UnityWebRequest www = UnityWebRequest.Post("http://192.168.1.10:9999", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Form Upload Complete!");
        }
    }


    public void SendMessageToServer()
    {
        if (_client.IsConnected)
        {
            //string message = MessageInputField.text;
            string message = tmp_input.text;
            if (message.StartsWith("!ping"))
            {
                message += " " + (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
            }
            
            if (!string.IsNullOrEmpty(message))
            {
                bool ss = _client.SendMessage(message);
                Debug.Log(message);
                Debug.Log(ss);
                Debug.Log(cache);
                if (ss)
                {

                    //MessageInputField.text = string.Empty;
                    tmp_input.text = string.Empty;
                }
            }
        }
    }

    private void OnClientReceivedMessage(TCPTestServer.ServerMessage message)
    {
        string finalMessage = ProcessServerMessage(message);
        if (message.Data == "Audio Clip Sent")
        {
            Debug.Log("Expect Audio");
        }
        lock (cacheLock)
        {
            if (string.IsNullOrEmpty(cache))
            {
                cache = string.Format("<color=green>{0}</color>\n", finalMessage);
            }
            else
            {
                cache += string.Format("<color=green>{0}</color>\n", finalMessage);
            }
        }
    }
    
    private void OnClientLog(string message)
    {
        lock (cacheLock)
        {
            if (string.IsNullOrEmpty(cache))
            {
                cache = string.Format("<color=grey>{0}</color>\n", message);
            }
            else
            {
                cache += string.Format("<color=grey>{0}</color>\n", message);
            }
        }
    }

    private void OnServerReceivedMessage(string message)
    {
        lock (cacheLock)
        {
            if (string.IsNullOrEmpty(cache))
            {
                cache = string.Format("<color=red>{0}</color>\n", message);
            }
            else
            {
                cache += string.Format("<color=red>{0}</color>\n", message);
            }
        }
    }

    private string ProcessServerMessage(TCPTestServer.ServerMessage message)
    {
        string data = message.Data;
        
        if (message.Data.StartsWith("!"))
        {
            string[] split = data.Split(' ');
            switch (split[0])
            {
                case "!ping":
                    double sentTimeStamp = double.Parse(split[1]);
                    double recTimeStamp = double.Parse(split[2]);
                    double nowTimeStamp = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
                    double toServerTime = recTimeStamp - sentTimeStamp;
                    double fromServerTime = nowTimeStamp - recTimeStamp;
                    double totalTime = nowTimeStamp - sentTimeStamp;
                    data = string.Format("!ping To Server: ({2}ms) {0}ms From Server: {1}",
                        toServerTime.ToString("F2"),
                        fromServerTime.ToString("F2"),
                        totalTime.ToString("F2"));
                    break;
            }
        }

        return string.Format("{0}: {1}", message.SenderData.Name, data);
    }

    private void OnClientConnected(TCPTestClient client)
    {
        clients.Add(client);
    }
    
    private void OnClientDisconnected(TCPTestClient client)
    {
        clients.Remove(client);
    }
}
