import socket
import threading
import json
import os
from flask import Flask, request, send_from_directory
from gevent.pywsgi import WSGIServer
IP = "192.168.1.10"
PORT = 9999

# set the project root directory as the static folder
app = Flask(__name__)
@app.route('/', methods=['POST'])
def DownloadFile():
    # request.form to get form parameter
    audFile = request.files["audFile"].read()
    outF = open("myOutFile.wav", "wb")
    outF.write(audFile)
    return ''

class SocketServer(socket.socket):
    clients = []
    
    def __init__(self):
        socket.socket.__init__(self)
        #To silence- address occupied!!
        self.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.bind(('192.168.1.10', 8052))
        self.listen(5)
        self.thread = None

    def run(self):
        print("Server started")
        try:
            self.accept_clients()
        except Exception as ex:
            print(ex)
        finally:
            print("Server closed")
            for client in self.clients:
                client.close()
            self.close()

    def accept_clients(self):
        client_count = 1
        while 1:
            (clientsocket, address) = self.accept()
            #Adding client to clients list
            self.clients.append(clientsocket)
            #Client Connected
            self.onopen(clientsocket,client_count)
            id = client_count
            client_count+=1
            #Receiving data from client
            threading.Thread(target=self.recieve, args=(clientsocket,id)).start()

    def recieve(self, client,id):
        while 1:
            data = client.recv(1024)
            if data == b'':
                break
            #Message Received
            print(data)
            self.onmessage(client, data,id)
        #Removing client from clients list
        self.clients.remove(client)
        #Client Disconnected
        self.onclose(client)
        #Closing connection with client
        client.close()
        #Closing thread
        #thread.join()
        print(self.clients)

    def broadcast(self, message,id):
        #Sending message to all clients
        #s = bytes("""{"SenderData":{"ID":3,"Name":"User3"},"Data":"Hi Default Message on Oculus Quest"}""", 'utf-8')
        data = {}
        data["SenderData"] = {"ID":str(id),"Name":"User" + str(id)}
        data["Data"] = message.decode("utf-8")
        d = json.dumps(data)
        print(d)
        s = bytes(d,"utf-8")
        for client in self.clients:
            client.send(s)

    def onopen(self, client,id):
        #s = bytes("""{"SenderData":{"ID":10,"Name":"User13"},"Data":"Client Connected"}""", 'utf-8') 
        data = {}
        data["SenderData"] = {"ID":str(id),"Name":"User" + str(id)}
        data["Data"] = b"Client Connected".decode("utf-8")
        d = json.dumps(data)
        print(d)
        s = bytes(d,"utf-8")
        client.send(s)
        print("Connected with " + str(client.getpeername()))

    def onmessage(self, client, message,id):
        self.broadcast(message,id)

    def onclose(self, client):
        pass

def main():
    server = SocketServer()
    server.run()
def flask_main():
    http_server = WSGIServer((IP, PORT), app)
    http_server.serve_forever()

if __name__ == "__main__":
    threading.Thread(target=main).start()
    threading.Thread(target=flask_main).start()
    
    